import React, {Component} from 'react';
import './App.css';
import {Route, Switch} from 'react-router-dom';
import Header from './containers/Header';
import LoginForm from './containers/Login';
import Home from './containers/Home';
import Posts from './containers/Posts';
import Albums from './containers/Albums';
import Todos from './containers/Todos';

class App extends Component {
  state = {
    validUser: true,
    username: null,
    company: null,
    website: null,
    userId: 3,
    users: []
  }

  componentWillMount() {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(data => {
        this.setState({users: data})
      })
  }

  updateUsername = (e) => {
    this.setState({
      username: e.target.value
    })
  }

  login = (e) => {
    e.preventDefault();
    let usersArray = this.state.users.slice();
    let user = usersArray.filter(user => user.username === this.state.username)

    if (user.length === 1) {
      this.setState({
        validUser: true,
        userId: user[0].id,
        company: user[0].company.name,
        website: user[0].website
      });
    }
  }

  logout = (e) => {
    this.setState({
      validUser: false
    })
  }

  render() {
    const {
      validUser,
      username,
      company,
      website,
      userId
    } = this.state
    return (

      <div className="App container">

        {validUser ?
          <Header
            username={username}
            company={company}
            website={website}
            logout={this.logout}
          /> : <div></div>
        }
        {!validUser ?
          <div className="row align-items-center">
            <div className="col">
              <LoginForm
                updateUsername={this.updateUsername}
                login={this.login}
                validUser={validUser}
              />
            </div>
          </div> :
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/posts" render={() =>
              <Posts userId={userId}/>
            }/>
            <Route path="/Albums" render={() =>
              <Albums userId={userId}/>
            }/>
            <Route path="/Todos" render={() =>
              <Todos userId={userId}/>
            }/>
          </Switch>
        }

      </div>
    );
  }
}

export default App;

