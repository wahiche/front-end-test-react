import React from 'react';

const Username = (props) => {
  return (
    <div className="col-6">
      <div className="row form-group justify-content-center">
        <div className="col-12">
          <input onChange={props.updateUsername} value={props.username} type="text" className="form-control" id="username" aria-describedby="enter username here" placeholder="Enter Username" />
        </div>
        <div className="col-12 mt-1 text-right">
          <button className="btn btn-primary" onClick={props.login}>Submit</button>
        </div>
      </div>
    </div>
  )
};

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isValid: props.validUser
    }
  }

  render() {
    const {
      login,
      updateUsername
    } = this.props
    return (
      <div>
        <h2 className="text-center">Login Here</h2>
        <form className="login-form mt-5">
          <div className="row justify-content-center">
            <Username login={login} updateUsername={updateUsername} />
          </div>
        </form>
      </div>
    )
  }
}

export default LoginForm;