import React from 'react';
import { NavLink } from 'react-router-dom';

const MenuItems = () => {
  return (
    <div className="navbar-nav">
      <NavLink activeClassName="active" className={"nav-item nav-link"} to="/posts">Posts <span className="sr-only">(current)</span></NavLink>
      <NavLink activeClassName="active" className="nav-item nav-link" to="/albums">Albums</NavLink>
      <NavLink activeClassName="active" className="nav-item nav-link" to="/todos">Todos</NavLink>
    </div>
  );
};

class Header extends React.Component {
  render() {
    return (
      <header className="header">
        <nav className="navbar navbar-expand-lg navbar-light bg-light justify-content-between">
          <a className="navbar-brand" href="/">React Learning</a>
          <MenuItems />
          <div className="user-info">
            <span>{this.props.username} <a href={`http://${this.props.website}`}>{this.props.company}</a></span>
            <button type="button" className="btn btn-success btn-sm ml-3" onClick={this.props.logout}>Logout</button>
          </div>
        </nav>
      </header>
    );
  }
}

export default Header;