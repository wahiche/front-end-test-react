import React from 'react';

const Sort = (props) => {
  return (
    <div className="sort text-right" style={{margin: '10px 0'}}>
      {props.sortDirection === 'asc' ?
        <i className="fa fa-sort-alpha-asc fa-2x rounded-circle p-3" onClick={props.sortAlbums} aria-hidden="true"
           style={{color: 'white', backgroundColor: 'rebeccapurple'}}></i>
        :
        <i className="fa fa-sort-alpha-desc fa-2x rounded-circle p-3" onClick={props.sortAlbums} aria-hidden="true"
           style={{color: 'white', backgroundColor: 'rebeccapurple'}}></i>
      }
    </div>
  )
}

const AlbumListing = (props) => {
  return (
    <ul className="list-group d-flex" style={{flex: "1 0 25%"}}>
      {
        props.userAlbums.map(item =>
          <li key={item.id} className="list-group-item" onClick={props.showPhotos} data-album-id={item.id}>
            {item.title}
          </li>
        )
      }
    </ul>
  )
}

const ImageListing = (props) => {

  return (
    <div className="images d-flex flex-wrap" style={{flex: "0 0 600px", marginLeft: '20px'}}>
      {
        props.userPhotos.map((image, index) =>
          <div key={index}>
            <img src={image.thumbnailUrl} alt=""/>
          </div>
        )
      }
    </div>
  )
}

class Albums extends React.Component {
  state = {
    photos: [],
    userAlbums: [],
    userPhotos: [],
    sortedAlbums: [],
    sortDirection: 'asc'
  }

  componentWillMount() {
    fetch("https://jsonplaceholder.typicode.com/photos")
      .then(response => response.json())
      .then(data => {
        this.setState({
          photos: data
        })
      })
    fetch("https://jsonplaceholder.typicode.com/albums")
      .then(response => response.json())
      .then(data => {
        let userAlbum = data.filter(album => album.userId === this.props.userId);
        this.setState({
          userAlbums: userAlbum,
          sortedAlbums: userAlbum
        })
      })

  }

  showPhotos = (e) => {
    e.preventDefault();
    let userPhotosArray = this.state.photos.slice();
    let userPhotosResult = userPhotosArray.filter(userPhoto => userPhoto.albumId === parseInt(e.target.dataset.albumId, 10));
    this.setState({
      userPhotos: userPhotosResult
    })
  }

  sortAlbum = () => {
    console.log("sort");
    let sortedAlbums = this.state.sortDirection === 'asc' ?
      this.state.userAlbums.sort((a, b) => a.title < b.title) :
      this.state.userAlbums.sort((a, b) => a.title > b.title);
    this.setState(prevState => (
      {
        sortedAlbums: sortedAlbums,
        sortDirection: prevState.sortDirection === 'asc' ? 'desc' : 'asc'
      }
    ))
  }

  render() {

    return (
      <div className="d-flex flex-column">
        <Sort sortAlbums={this.sortAlbum} sortDirection={this.state.sortDirection}/>
        <div className="d-flex">
          <AlbumListing userAlbums={this.state.sortedAlbums} showPhotos={this.showPhotos}/>
          <ImageListing userPhotos={this.state.userPhotos}/>
        </div>
      </div>
    );
  }
}

export default Albums;