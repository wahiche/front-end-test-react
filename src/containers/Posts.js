import React from 'react';

const SearchComponent = (props) => {
  return (
    <div className="input-group mt-3 mb-3">
      <input className="form-control text-center" style={{fontSize: '36px', color: 'rebeccapurple'}} type="text" placeholder="Search..." onChange={props.updateSearch} />
    </div>
  );
};

const Post = (props) => {
  return (
    <div className="list-group ">
      {
        props.posts.map((post, id) =>
          <div key={id}>
            <div className="list-group-item d-flex flex-column align-items-start">
              <div>
                <h5><a href={`#panel${id}`} role="button" data-toggle="collapse" style={{color: 'rebeccapurple', textTransform: 'uppercase'}}>{post.title}</a></h5>
              </div>
              <div className={`collapse ${id === 0 ? "show" : ""}`} id={`panel${id}`} data-parent="#accordion">
                <div>{post.body}</div>
              </div>
              <div className="btn btn-primary mt-3">
                Comments <span className="badge badge-light">{props.commentCount}</span>
              </div>
            </div>
          </div>
        )
      }

    </div>
  );
}

class Posts extends React.Component {
  state = {
    posts: [],
    commentCount: 0,
    searchResults: [],
    searchTerm: ''
  }

  componentWillMount() {
    fetch(`https://jsonplaceholder.typicode.com/posts?userId=${this.props.userId}`)
      .then(response => response.json())
      .then(data => {
        this.setState({
          posts: data
        })
        this.commentCount()
      })
  }

  commentCount() {
    fetch(`https://jsonplaceholder.typicode.com/posts/${this.props.userId}/comments`)
      .then(response => response.json())
      .then(data => this.setState({ commentCount: data.length }))
  }

  updateSearch = (e) => {
    this.setState({
      searchTerm: e.target.value.toLowerCase()
    })
  }

  render() {
    let filteredPosts = this.state.posts.filter(post =>
      post.body.toLowerCase().includes(this.state.searchTerm) || post.title.toLowerCase().includes(this.state.searchTerm)
    )

    return (
      <div>
        <SearchComponent updateSearch={this.updateSearch} />
        <div id="accordian">
          <Post posts={filteredPosts} commentCount={this.state.commentCount} />
        </div>
      </div>
    );
  }
}

export default Posts;