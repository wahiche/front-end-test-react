import React from 'react';

class Home extends React.Component {
  render() {
    return (
      <div className="cards d-flex flex-column mb-5">
        <div className="card mt-3 rounded-0 ">
          <div className="card-header text-white bg-primary rounded-0">
            <h5 className="card-title mb-0">Posts</h5>
          </div>
          <div className="card-body">
            <ul className="card-text list-group list-group-flush">
              <li className="list-group-item">Display list of Posts (titles), click / expandable to display the body, display (count of comments) on the posts</li>
              <li className="list-group-item">Have a text search box: text search, display only posts that have matching full-word text either on title or body</li>
            </ul>
          </div>
        </div>
        <div className="card mt-3 rounded-0">
          <div className="card-header text-white bg-success rounded-0">
            <h5 className="card-title mb-0">Albums</h5>
          </div>
          <div className="card-body">
            <ul className="card-text list-group list-group-flush">
              <li className="list-group-item">Display list of Albums (titles) and (count) of photos in the album</li>
              <li className="list-group-item">Ability to sort albums alphabetically by title</li>
              <li className="list-group-item">Link to “View Photos” which will pop-up a modal window that will display all the thumbnails of photos belong to that album</li>
            </ul>
          </div>
        </div>
        <div className="card mt-3 rounded-0">
          <div className="card-header text-white bg-info rounded-0">
            <h5 className="card-title mb-0">Todos</h5>
          </div>
          <div className="card-body">
            <ul className="card-text list-group list-group-flush">
              <li className="list-group-item">Display list of To Dos (titles) for the user</li>
              <li className="list-group-item">Ability to toggle: include / exclude completed</li>
              <li className="list-group-item">Completed task displayed with a checkbox, uncompleted task displayed with empty box</li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;