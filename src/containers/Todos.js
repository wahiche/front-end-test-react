import React from 'react';
import './Todos.css';

const ToggleChecked = (props) => {
  return (
    <button className="btn btn-info mt-3 mb-3 align-self-end" type="button" onClick={props.toggleCompleted}>
      {
        props.showCompleted ? "Hide Completed" : "Show Completed"
      }
    </button>
  )
}

const Todo = (props) => {
  return (
    <ul className="list-group">
      {
        props.todosArray.map(todo =>
          <li key={todo.id} className={`list-group-item d-flex align-items-center ${todo.completed ? 'active' : ''}`} data-id={todo.id}
              onClick={props.updateCompleted}>
            {!todo.completed ?
              <i className="fa fa-square-o fa-2x" aria-hidden="true"></i> :
              <i className="fa fa-check fa-2x" aria-hidden="true"></i>
            }
            <div className="ml-3" style={{pointerEvents: 'none'}}>{todo.title}</div>
          </li>
        )
      }
    </ul>
  )
}

class Todos extends React.Component {
  state = {
    todos: [],
    showCompleted: true
  }

  componentWillMount() {
    fetch(`https://jsonplaceholder.typicode.com/todos`)
      .then(response => response.json())
      .then(data => {
        let userTodos = data.filter(userTodo => userTodo.userId === this.props.userId)
        this.setState({
          todos: userTodos
        })
      })
  }

  render() {
    let todosArray = [...this.state.todos.slice()];
    let toggledTodos = todosArray.filter(todo => !todo.completed)

    const updateCompleted = (e) => {
      let results = todosArray.map((todo) => {
        if (todo.id === parseInt(e.target.dataset.id, 10)) {
          console.log(todo)
          this.setState({
            todos: this.state.todos.map(
              (el) => el.id === parseInt(e.target.dataset.id, 10) ? Object.assign({}, el, {completed: !el.completed}) : el
            )
          });
        }
        return todo
      })
      return results
    }

    const toggleCompleted = () => {
      this.setState(prevState => ({
        showCompleted: !prevState.showCompleted
      }), () => toggledTodos);
    }


    return (
      <div className="d-flex flex-column">
        <ToggleChecked showCompleted={this.state.showCompleted} toggleCompleted={toggleCompleted}/>
        <Todo todosArray={this.state.showCompleted ? todosArray : toggledTodos} checked={todosArray}
              updateCompleted={updateCompleted}
              showCompleted={this.state.showCompleted}/>

      </div>
    );
  }
}

export default Todos;